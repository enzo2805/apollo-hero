import { Hero } from './hero';

export const HEROES: Hero[] = [
    {id: 11, name: 'Underlord'},
    {id: 12, name: 'Wraith King'},
    {id: 13, name: 'Dark Willow'},
    {id: 14, name: 'Invoker'},
    {id: 15, name: 'Windrunner'},
    {id: 16, name: 'Mirana'},
    {id: 17, name: 'Dragon Knight'},
    {id: 18, name: 'Phantom Assassin'},
    {id: 19, name: 'Pangolier'},
    {id: 20, name: 'Magnus'},
    {id: 21, name: 'Necromancer'},
];