import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Apollo, gql } from 'apollo-angular';

const HEROES = gql`
  query Heroes {
    heroes {
      id
      name
    }
  }
`;

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {

  constructor(private apollo: Apollo) { }

  getHeroes(): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: HEROES,
    }).valueChanges;
  }
}
